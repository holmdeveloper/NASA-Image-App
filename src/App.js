import React from "react";
import "./CSS/App.css";

import MarsImages from "./MarsImages";

import AstronomyPictureOfTheDay from "./AstronomyPictureOfTheDay"; // Importera din nya komponent

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Welcome to Space Exploration</h1>
        <p>Explore images from NASA and the adventures of the Mars rover</p>
        <nav className="App-menu">
          <ul>
            <li>
              <a href="#aspod">Astronomy Picture Of The Day</a>
            </li>
            <li>
              <a href="#mars">Mars-pictures</a>
            </li>
          </ul>
        </nav>
      </header>

      <div className="Content">
        <div className="NasaImages" id="aspod">
          <AstronomyPictureOfTheDay />
        </div>

        <div className="MarsImages" id="mars">
          <MarsImages />
        </div>
      </div>
    </div>
  );
}

export default App;
