import React, { useEffect, useState } from "react";
import axios from "axios";
import "./CSS/AstronomyPictureOfTheDay.css";

const AstronomyPictureOfTheDay = () => {
  const [apodData, setApodData] = useState(null);

  useEffect(() => {
    const apiKey = "UX8Nptl55EeAhAUB6Z6W9QLKFplnXDcKq8STvtwZ"; // Ersätt med din riktiga API-nyckel

    // Gör en GET-förfrågan till NASA's APOD API
    axios
      .get(`https://api.nasa.gov/planetary/apod?api_key=${apiKey}`)
      .then((response) => {
        setApodData(response.data);
      })
      .catch((error) => {
        console.error("Fel vid hämtning av APOD-data:", error);
      });
  }, []);

  return (
    <div className="AstronomyPictureOfTheDay">
      <h2>Astronomy Picture of the Day</h2>
      {apodData ? (
        <div>
          <h3>{apodData.title}</h3>
          <p>{apodData.date}</p>
          {apodData.media_type === "image" ? (
            // Visa en bild om det är en bild
            <img src={apodData.url} alt={apodData.title} />
          ) : apodData.media_type === "video" ? (
            // Visa en inbäddad video om det är en video
            <iframe
              title={apodData.title}
              width="560"
              height="315"
              src={apodData.url}
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
            ></iframe>
          ) : (
            <p>No Support.</p>
          )}
          <p>{apodData.explanation}</p>
        </div>
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
};

export default AstronomyPictureOfTheDay;
