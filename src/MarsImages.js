import React, { useEffect, useState } from "react";
import axios from "axios";
import "./CSS/MarsImages.css";

const MarsImages = () => {
  const [marsImages, setMarsImages] = useState([]);
  const [selectedRover, setSelectedRover] = useState("curiosity");

  const [currentPage, setCurrentPage] = useState(1);
  const imagesPerPage = 1;

  useEffect(() => {
    const apiKey = "UX8Nptl55EeAhAUB6Z6W9QLKFplnXDcKq8STvtwZ";

    axios
      .get(
        `https://api.nasa.gov/mars-photos/api/v1/rovers/${selectedRover}/photos?sol=1000&api_key=${apiKey}`
      )
      .then((response) => {
        setMarsImages(response.data.photos);
      })
      .catch((error) => {
        console.error("Fel vid hämtning av Mars-bilder:", error);
      });
  }, [selectedRover]);

  const handleRoverChange = (event) => {
    setSelectedRover(event.target.value);
    setCurrentPage(1);
  };

  const indexOfLastImage = currentPage * imagesPerPage;
  const indexOfFirstImage = indexOfLastImage - imagesPerPage;
  const currentImages = marsImages.slice(indexOfFirstImage, indexOfLastImage);

  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  return (
    <div className="MarsImages">
      <h2>Mars-pictures</h2>
      <div className="rover-dropdown">
        <label htmlFor="rover-select">Choose a mission:</label>
        <select
          id="rover-select"
          value={selectedRover}
          onChange={handleRoverChange}
        >
          <option value="curiosity">Curiosity</option>
          <option value="opportunity">Opportunity</option>
          <option value="spirit">Spirit</option>
        </select>
      </div>
      <div className="gallery">
        {currentImages.map((image) => (
          <img
            key={image.id}
            src={image.img_src}
            alt={`Mars Rover - ${image.id}`}
          />
        ))}
      </div>
      <div className="pagination">
        <button onClick={() => paginate(currentPage - 1)}>Back</button>
        <button onClick={() => paginate(currentPage + 1)}>Forward</button>
      </div>
    </div>
  );
};

export default MarsImages;
